import 'package:flutter/material.dart';
import 'views.dart';

class IrregularBottomAppBar extends StatefulWidget {
  @override
  _IrregularBottomAppBarState createState() => _IrregularBottomAppBarState();
}

class _IrregularBottomAppBarState extends State<IrregularBottomAppBar> {

  List<Widget> _eachView;
  int _index=0;

  @override
  void initState() {
    super.initState();
    _eachView=List();
    _eachView
      ..add(AllViews('Home'))
      ..add(AllViews('Air'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _eachView[_index],
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context){
            return AllViews('New Page');
          }));
        },
        tooltip: 'Notification',
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
      
      // integrate bottom navigation bar with float action button
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      bottomNavigationBar: BottomAppBar(
        color: Colors.lightBlue,
        shape: CircularNotchedRectangle(),
        // bottom bar on the both sides of FloatingActionButton
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.home),
              color: Colors.white,
              onPressed: (){
                setState(() {
                 _index=0; 
                });
              },
            ),
            IconButton(
              icon: Icon(Icons.airplanemode_active),
              color: Colors.white,
              onPressed: (){
                setState(() {
                 _index=1; 
                });
              },
            )
          ],
        ),
      ),
    );
  }
}