import 'package:flutter/material.dart';

class AllViews extends StatefulWidget {
  String  _title;
  AllViews(this._title);

  @override
  _AllViewsState createState() => _AllViewsState();
}

class _AllViewsState extends State<AllViews> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._title),
      ),
      body: Center(
        child: Text(widget._title),
      ),
    );
  }
}