import 'package:flutter/material.dart';
import 'irregular_bottom_appbar.dart';

void main()=>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Irregular bottom navigator',
      //self-define themes
      theme: ThemeData(
        primarySwatch: Colors.lightBlue
      ),
      home: IrregularBottomAppBar(),
    );
  }
}